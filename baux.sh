#!/usr/bin/env bash

### Script to setup a working media-setup of
### applications: rclone VFS, Plexdrive, MergerFS and UnionFS
### for use with Plex, Sonarr and Radarr.

# shellcheck source=func/create_dir.sh
source "${BASH_SOURCE%/*}/func/create_dir.sh"
# shellcheck source=func/install_drive.sh
source "${BASH_SOURCE%/*}/func/install_drive.sh"
# shellcheck source=func/mount_drive.sh
source "${BASH_SOURCE%/*}/func/mount_drive.sh"
# shellcheck source=func/mount_fuse.sh
source "${BASH_SOURCE%/*}/func/mount_fuse.sh"
# shellcheck source=func/remove_everything.sh
source "${BASH_SOURCE%/*}/func/remove_everything.sh"
# shellcheck source=func/upload_data.sh
source "${BASH_SOURCE%/*}/func/upload_data.sh"
# shellcheck source=func/upload_logs.sh
source "${BASH_SOURCE%/*}/func/upload_logs.sh"

function invokeFunc () {

    choice="${1}"

    [[ -z ${choice} ]] && echo -e "No action specified.\\nInvalid Usage. Pass either '-h' or '--help' to see usage." && exit 1

    case ${choice} in
        "-c"|"--create-dirs")
            makeBaseDirStruct

            case "${2}" in
                "rclone")
                    makeRcloneDirStruct
                    ;;
                "plexdrive")
                    makePlexdriveDirStruct
                    ;;
            esac

            case "${2}" in
                "mergerfs")
                    makeMergerFSDirStruct
                    ;;
                "unionfs")
                    makeUnionFSDirStruct
                    ;;
            esac
            ;;
        "-i"|"--install-apps")
            case "${2}" in
                "rclone")
                    installRclone
                    ;;
                "plexdrive")
                    installPlexdrive
                    ;;
            esac
            ;;
        "-m"|"--mount-fuse")
            case "${2}" in
                "mergerfs")
                    mergerfsMount
                    ;;
                "unionfs")
                    unionfsMount
                    ;;
            esac
            ;;
        "-r"|"--mount-drive")
            case "${2}" in
                "rclone")
                    rcloneMount
                    ;;
                "plexdrive")
                    plexdriveMount
                    ;;
            esac
            ;;
        "-u"|"--upload-data")
            rcloneUpload
            ;;
        "-l"|"--upload-logs")
            subChoice="${2}"

            case "${subChoice}" in
                'rclone')
                    moveRcloneLogs
                    ;;
                'plexdrive')
                    movePlexdriveLogs
                    ;;
                'mergerfs')
                    moveMergerFSLogs
                    ;;
                'unionfs')
                    moveUnionFSLogs
                    ;;
                *)
                    invokeFunc --help
                    ;;
            esac
            ;;
        "-h"|"--help")
            echo -e "\\nUtility: baux.sh \\n\
            \\rAuthor: K Gopal Krishna \\n\
            \\rLicense: BSD-2-Clause / Simplified BSD License / FreeBSD License \\n\
            \\nUsage: \\n\
                    \\r\\t-n, --non-interactive: Work through commandline options. This flag is mandatory if you want to use the proceeding options. \\n\
                    \\r\\t-r, --mount-drive: Mount Google Drive using either 'rclone' or 'plexdrive'. \\n\
                    \\r\\t-m, --mount-fuse: Merge local and cloud filesystems using either 'mergerfs' or 'unionfs'. \\n\
                    \\r\\t-u, --upload-data: Upload locally downloaded files to cloud. \\n\
                    \\r\\t-l, --upload-logs ( 'rclone' | 'mergerfs' ): Upload logs to cloud. \\n\
                    \\r\\t-h, --help: Display this message.\\n"
            ;;
        *)
            echo -e "\\nInvalid usage. Use '-h' or '--help' for more info.\\n"
            ;;
    esac

}

function main () {

    whiptail --title "INFORMATION" --yesno "This script makes the following assumptions:\\n\\n1. You are running either Debian Stretch or Ubuntu 16.04+.\\n2. You have whiptail, crontab and FUSE installed.\\n3. You have set user_allow_other in /etc/fuse.conf.\\n4. You have read the instructions described in the README.\\n\\nDo you want to continue?" 20 80 || exit

    while true; do
        firstPrompt=$(whiptail --title "Aye! Let's get this setup." --menu "\\nMake the right choice." 20 60 10 \
            "1." "Get this shit setup as fast as you can." \
            "2." "I want to see this through." \
            "3." "I want this abomination off of my filesystem." 3>&1 1>&2 2>&3)

        case "${firstPrompt}" in
            "1.")
                invokeFunc --create-dirs "rclone" "mergerfs"
                invokeFunc --install-apps "rclone"
                invokeFunc --mount-drive "rclone"
                invokeFunc --mount-fuse "mergerfs"
                ;;
            "2.")
                while true; do
                    secondPrompt=$(whiptail --title "Tools" --menu "What do you wanna setup?" 25 78 16 \
                        "0." "Go back" \
                        "1." "Mount Utility" \
                        "2." "FUSE Utility" \
                        "3." "Exit" 3>&1 1>&2 2>&3)

                    case ${secondPrompt} in
                        "0.")
                            break
                            ;;
                        "1.")
                            while true; do
                                thirdPrompt=$(whiptail --title "Mount Utility" --menu "\\nPick your poison." 25 78 16 \
                                    "0." "Go back" \
                                    "1." "RClone" \
                                    "2." "Plexdrive" \
                                    "3." "Exit" 3>&1 1>&2 2>&3)

                                case "${thirdPrompt}" in
                                    "0.")
                                        break
                                        ;;
                                    "1.")
                                        while true; do
                                            fourthPrompt=$(whiptail --title "Mount Utility" --menu "\\nWhat do you wanna do?" 25 78 16 \
                                            "0." "Go back." \
                                            "1." "Create directories for RClone." \
                                            "2." "Install and setup Rclone." \
                                            "3." "Mount drive with Rclone." \
                                            "4." "Setup crontab for checking mounts as well as data and log uploads." \
                                            "5." "Exit." 3>&1 1>&2 2>&3)

                                            case "${fourthPrompt}" in
                                                "0.")
                                                    break
                                                    ;;
                                                "1.")
                                                    invokeFunc --create-dirs "rclone"
                                                    ;;
                                                "2.")
                                                    invokeFunc --install-apps "rclone"
                                                    ;;
                                                "3.")
                                                    invokeFunc --mount-drive "rclone"
                                                    ;;
                                                "5.")
                                                    exit
                                                    ;;
                                            esac
                                        done
                                        ;;
                                    "2.") 
                                        while true; do
                                            fourthPrompt=$(whiptail --title "Mount Utility" --menu "\\nWhat do you wanna do?" 25 78 16 \
                                            "0." "Go back." \
                                            "1." "Create directories for Plexdrive." \
                                            "2." "Install and setup Plexdrive." \
                                            "3." "Mount drive with Plexdrive." \
                                            "4." "Setup crontab for checking mounts as well as data and log uploads." \
                                            "5." "Exit." 3>&1 1>&2 2>&3)

                                            case "${fourthPrompt}" in
                                                "0.")
                                                    break
                                                    ;;
                                                "1.")
                                                    invokeFunc --create-dirs "plexdrive"
                                                    ;;
                                                "2.")
                                                    invokeFunc --install-apps "plexdrive"
                                                    ;;
                                                "3.")
                                                    invokeFunc --mount-drive "plexdrive"
                                                    ;;
                                                "5.")
                                                    exit
                                                    ;;
                                            esac
                                        done
                                        ;;
                                    "3.")
                                        exit
                                        ;;
                                esac
                            done
                            ;;
                        "2.")
                            while true; do
                                thirdPrompt=$(whiptail --title "FUSE Utility" --menu "\\nPick your venom:" 25 78 16 \
                                    "0." "Go back" \
                                    "1." "MergerFS" \
                                    "2." "UnionFS" \
                                    "3." "Exit" 3>&1 1>&2 2>&3)

                                case ${thirdPrompt} in
                                    "0.")
                                        break
                                        ;;
                                    "1.")
                                        while true; do
                                            fourthPrompt=$(whiptail --title "FUSE Utility" --menu "What do you wanna do?" 25 78 16 \
                                                "0." "Go back." \
                                                "1." "Create directories for MergerFS" \
                                                "2." "Merge paths with MergerFS." \
                                                "3." "Setup crontab for checking mounts and uploading logs." \
                                                "4." "Exit." 3>&1 1>&2 2>&3)

                                            case "${fourthPrompt}" in
                                                "0.")
                                                    break
                                                    ;;
                                                "1.")
                                                    invokeFunc --create-dirs "mergerfs"
                                                    ;;
                                                "2.")
                                                    invokeFunc --mount-fuse "mergerfs"
                                                    ;;
                                                "4.")
                                                    exit
                                                    ;;
                                            esac
                                        done
                                        ;;
                                    "2.")
                                        while true; do
                                            fourthPrompt=$(whiptail --title "FUSE Utility" --menu "What do you wanna do?" 25 78 16 \
                                                "0." "Go back." \
                                                "1." "Create directories for UnionFS" \
                                                "2." "Merge paths with UnionFS." \
                                                "3." "Setup crontab for checking mounts and uploading logs." \
                                                "4." "Exit." 3>&1 1>&2 2>&3)

                                            case "${fourthPrompt}" in
                                                "0.")
                                                    break
                                                    ;;
                                                "1.")
                                                    invokeFunc --create-dirs "unionfs"
                                                    ;;
                                                "2.")
                                                    invokeFunc --mount-fuse "unionfs"
                                                    ;;
                                                "4.")
                                                    exit
                                                    ;;
                                            esac
                                        done
                                        ;;
                                    "3.")
                                        exit
                                        ;;
                                esac
                            done
                            ;;
                        "3.")
                            exit
                            ;;
                    esac
                done
                ;;
            "3.")
                nukeAllChanges && whiptail --title "Deletion" --msgbox "Reverted all changes. Press OK to exit." 8 78
                exit
                ;;
        esac
    done

}

# Check if the first arg is empty, complexity is because of presetting -u
(set +u && [[ -z "${1}" ]] && set -- "-h") || set -u

case "${1}" in
    "-n"|"--non-interactive")
        (set +u && [[ -z "${2}" ]] && set -u && invokeFunc --help) || (set +u && [[ -z "${3}" ]] && invokeFunc --help) || (set -u && invokeFunc "${2}" "${3}")
        ;;
    "-i"|"--interactive")
        main
        ;;
    *)
        invokeFunc --help
        ;;
esac

exit 0
