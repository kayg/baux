# Allow aliases to work inside the script, produce errors if globbing fails
shopt -s expand_aliases failglob
# Exit the whole script if one command fails, don't use unset variables and exit script if any command in a pipeline errors out.
set -eu -o pipefail

### Global Aliases
# Set time, day and script name
alias TIME='date "+%d/%m/%Y %T"'
alias DAY='date "+%d-%b-%Y"'
alias S_NAME='basename "${0}"'

# Set alias for rclone
if command -v rclone; then
    alias rc='$(command -v rclone)'
else
    alias rc='${INSTALLDIR}/rclone'
fi

# Set alias for plexdrive
if command -v plexdrive; then
    alias pd='$(command -v plexdrive)'
else
    alias pd='${INSTALLDIR}/plexdrive'
fi


### Global variables
export BASEDIR="${HOME}/plex"

# Mount Directories
export DRIVE_DIR="${BASEDIR}/mounts/drive"
export FUSED_DIR="${BASEDIR}/mounts/fused"
export LOCAL_DIR="${BASEDIR}/mounts/local"

# Mount utilities
export PLEXDRIVE_CACHEDIR="${BASEDIR}/cache/plexdrive"
export PLEXDRIVE_CONFIGDIR="${BASEDIR}/config/plexdrive"
export PLEXDRIVE_LOGDIR="${BASEDIR}/logs/plexdrive"
export RCLONE_CACHEDIR="${BASEDIR}/cache/rclone"
export RCLONE_CONFIGDIR="${BASEDIR}/config/rclone"
export RCLONE_LOGDIR="${BASEDIR}/logs/rclone"

# FUSE utilities
export MERGERFS_LOGDIR="${BASEDIR}/logs/mergerfs"
export UNIONFS_LOGDIR="${BASEDIR}/logs/unionfs"

# Install directory
export INSTALLDIR="${BASEDIR}/bin"

# List of variables
export VAR_LIST=("${BASEDIR}" "${DRIVE_DIR}" "${FUSED_DIR}" "${LOCAL_DIR}" "${INSTALLDIR}")
export VAR_RC_MNT=("${RCLONE_CACHEDIR}" "${RCLONE_CONFIGDIR}" "${RCLONE_LOGDIR}")
export VAR_PD_MNT=("${PLEXDRIVE_CACHEDIR}" "${PLEXDRIVE_CONFIGDIR}" "${PLEXDRIVE_LOGDIR}")

# Global functions
function isAlive () {

    service="${1}"
    
    if pgrep -f "${service}"; then
        return 0
    else
        return 1
    fi

}
