# shellcheck source=../def/hash_include.sh
source "${BASH_SOURCE%/*}/../def/hash_include.sh"

function makeBaseDirStruct () {

    for dir in "${VAR_LIST[@]}"; do
        mkdir -p "${dir}"
    done

}

function makeRcloneDirStruct () {

    for dir in "${VAR_RC_MNT[@]}"; do
        mkdir -p "${dir}"
    done

}

function makePlexdriveDirStruct () {

    for dir in "${VAR_PD_MNT[@]}"; do
        mkdir -p "${dir}"
    done

}

function makeMergerFSDirStruct () {

    mkdir -p "${MERGERFS_LOGDIR}"

}

function makeUnionFSDirStruct () {

    mkdir -p "${UNIONFS_LOGDIR}"

}
