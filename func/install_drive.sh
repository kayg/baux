# shellcheck source=../def/hash_include.sh
source "${BASH_SOURCE%/*}/../def/hash_include.sh"

function installRclone () {

    curl -O https://downloads.rclone.org/rclone-current-linux-amd64.zip
    unzip rclone-current-linux-amd64.zip
    cd rclone-*-linux-amd64 || exit
    mv rclone "${INSTALLDIR}/" && rm -rf "rclone-*-linux-amd64"
    chmod 755 "${INSTALLDIR}/rclone"

}

function installPlexdrive () {

    curl -O https://github.com/dweidenfeld/plexdrive/releases/download/5.0.0/plexdrive-linux-amd64
    mv plexdrive-linux-amd64 "${INSTALLDIR}/plexdrive"
    chmod 755 "${INSTALLDIR}/plexdrive"

}
