# shellcheck source=../def/hash_include.sh
source "${BASH_SOURCE%/*}/../def/hash_include.sh"

function rcloneMount () {

    # Cache directory for rclone
    cacheDir="${RCLONE_CACHEDIR}"

    # Config file for rclone
    configFile="${RCLONE_CONFIGDIR}/rclone.conf"

    # Save logs of each day
    logFile="${RCLONE_LOGDIR}/$(DAY)-rclone.log"

    # Config directory for rclone

    # Check if process is alive
    if isAlive 'rclone mount'; then
        # If yes, print affirmation and exit.
        echo -e "\\nSCRIPT: $(S_NAME)\\nTIME: $(TIME)\\nSTATUS: Drive already mounted.\\nACTION: Exiting.\\n" >> "${logFile}"
		exit 0
	else
        # If not, print to log and proceed.
        echo -e "\\nSCRIPT: $(S_NAME)\\nTIME: $(TIME)\\nSTATUS: Drive not mounted.\\nACTION: Mounting...\\n" >> "${logFile}"
	fi

    # Mount Google Drive using Rclone VFS.
    rc mount gd: "${DRIVE_DIR}" \
   		--allow-other `# Allow non-root users to mount.` \
        --buffer-size 1G `# Amount of data to be buffered in advance. RAM consumption = buffer-size * number of open files.` \
        --cache-dir "${cacheDir}" `# Location for caching data` \
        --config "${configFile}" `# Location of the configuration file.` \
        --daemon `# Run in background.` \
   		--dir-cache-time 48h `# Time before cache should be refreshed / is invalid.` \
   		--drive-chunk-size 32M `# Size of each piece / chunk while uploading.` \
   		--log-level "INFO" `# Print what is necessary.` \
   		--log-file "${logFile}" `# Save logs to file. See expanded location below.` \
        --umask 002 `# Permissions of files on the remote filesystem: 777 - 002 = 775 (rwxrwxr-x).` \
		--vfs-cache-mode "off" `# Both reads and writes are direct to the remote.` \
 		--vfs-cache-max-age 5h `# Amount of time files should be kept in cache before they are removed.` \
   		--vfs-read-chunk-size 128M `# Read the source object in parts at the cost of higher requests.`  \
   		--vfs-read-chunk-size-limit off `# Do not grow read-chunk size after each request.` \
        --vfs-cache-poll-interval 1m `# Interval to poll cache for stale objects.` \
        --volname "Google Drive" || : # || because of set -u

    ### Rclone mounts the drive at /media/mounts/drive.
    ### -> With the permissions of a non-superuser.
    ### -> Buffers 1 Gigabyte in advance for each open file (and consumes memory accordingly).
    ### -> Caches data at /media/cache/rclone
    ### -> Runs in background.
    ### -> Refreshes its cache backend every 2 days.
    ### -> Uploads files in 32 Megabyte chunks.
    ### -> Writes necessary messages to /media/logs/rclone/DATE-rclone.log.
    ### -> Has permissions of 775 on each file on the cloud filesystem.
    ### -> Opens the cloud filesystem in "off" mode through its VFS mechanism which is
    ###    reading directly from remote and writing directly to the remote.
    ### -> Removes file after 5 hours of it being in the cache.
    ### -> Reads files from cloud in segments of 128 Megabyte.
    ### -> Does not grow the read chunk size after each read. (0-256, 256-512, 512-768, 768-1024....)
    ###    if limit ( > read size ) was specified; the requests would grow until the limit is reached.
    ###    For example, 100M and 500M would be: 0-100, 100-300, 300-700, 700-1200, 1200-1700...
    ### -> Asks every one minute, what files are new?
    ### -> With the volume name as "Google Drive".

    # If remont successful
    if isAlive 'rclone mount'; then
        # Print message and exit script with a code of 0.
		echo -e "\\nSCRIPT: $(S_NAME)\\nTIME: $(TIME)\\nSTATUS: Drive mounted succesfully.\\nACTION: Exiting.\\n" >> "${logFile}"
		exit 0
	else
        # If not, print to log file and exit script with a code of 1.
		echo -e "\\nSCRIPT: $(S_NAME)\\nTIME: $(TIME)\\nSTATUS: Drive mounting failed. Check log for details.\\nACTION: Exiting\\n" >> "${logFile}"
		exit 1
	fi

}

function plexdriveMount () {

    # Cache directory for plexdrive
    cacheFile="${PLEXDRIVE_CACHEDIR}/cache.bolt"

    # Config directory for plexdrive
    configDir="${PLEXDRIVE_CONFIGDIR}"

    # Save logs of each day
    logFile="${PLEXDRIVE_LOGDIR}/$(DAY)-plexdrive.log"

    # Check if process is alive
    if isAlive 'plexdrive'; then
        # If yes, print affirmation and exit.
        echo -e "\\nSCRIPT: $(S_NAME)\\nTIME: $(TIME)\\nSTATUS: Drive already mounted.\\nACTION: Exiting.\\n" >> "${logFile}"
		exit 0
	else
        # If not, print to log and proceed.
        echo -e "\\nSCRIPT: $(S_NAME)\\nTIME: $(TIME)\\nSTATUS: Drive not mounted.\\nACTION: Mounting...\\n" >> "${logFile}"
	fi

    (
    pd mount \
        --cache-file="${cacheFile}" \
        --chunk-check-threads=8 `# Use 8 threads for checking existence of chunks.` \
        --chunk-load-ahead=10 `# Number of chunks to be read ahead.` \
        --chunk-load-threads=8 `# Use 8 threads for downloading chunks.` \
        --chunk-size=12M `# Size of each chunk.` \
        --config="${configDir}" `# Configuration file` \
        --max-chunks=1600 `# The max number of chunks to be stored on RAM` \
        --refresh-interval=1m `# Poll cache every minute for changes.` \
        --verbosity=3 `# Be extra verbose about activity.` \
        "${DRIVE_DIR}" >> "${logFile}" 2>&1 || : # Redirect stderr to stdout and stdout to logfile. 
    ) & # Run in background.

    ### Plexdrive mount
    ### -> With /home/user/plex/cache/plexdrive/cache.bolt as cachefile.
    ### -> Use 8 threads to check chunk existence.
    ### -> Load 10 chunks beforehand.
    ### -> Use 8 threads for downloading chunks.
    ### -> Download chunks in 12 Megabyte segments.
    ### -> With /home/user/plex/config/plexdrive/ as config directory.
    ### -> Download upto 1600 chunks (upper limit).
    ### -> Refresh cache to check for changes every one minute.
    ### -> Print as much information as you can.
    ### At /home/user/plex/mounts/drive and log output to this directory: /home/user/plex/logs/plexdrive/

    # If remont successful
    if isAlive 'plexdrive'; then
        # Print message and exit script with a code of 0.
		echo -e "\\nSCRIPT: $(S_NAME)\\nTIME: $(TIME)\\nSTATUS: Drive mounted succesfully.\\nACTION: Exiting.\\n" >> "${logFile}"
		exit 0
	else
        # If not, print to log file and exit script with a code of 1.
		echo -e "\\nSCRIPT: $(S_NAME)\\nTIME: $(TIME)\\nSTATUS: Drive mounting failed. Check log for details.\\nACTION: Exiting\\n" >> "${logFile}"
		exit 1
	fi

}
