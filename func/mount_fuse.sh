# shellcheck source=../def/hash_include.sh
source "${BASH_SOURCE%/*}/../def/hash_include.sh"

function mergerfsMount () {

    # Location of log file.
    logFile="${MERGERFS_LOGDIR}/$(DAY)-mergerfs.log"

    # If process is alive and working
    if isAlive 'mergerfs' && [[ -f "${FUSED_DIR}/mountcheck" ]]; then
        # Print affirmation message and exit script with a code of 0.
		echo -e "\\nSCRIPT: $(S_NAME)\\nTIME: $(TIME)\\nSTATUS: Filesystems already merged.\\nACTION: Exiting.\\n" >> "${logFile}"
		exit 0
	else
        # If not, proceed.
		echo -e "\\nSCRIPT: $(S_NAME)\\nTIME: $(TIME)\\nSTATUS: Filesystems not merged.\\nACTION: Merging...\\n" >> "${logFile}"
	fi

	/usr/bin/mergerfs \
        -o defaults,allow_other,category.action=all,category.create=ff,dropcacheonclose=true,fsname=MergedFS,func.getattr=newest,hard_remove,noatime,sync_read,threads=8,umask=0002,use_ino \
		"${LOCAL_DIR}":"${DRIVE_DIR}" \
		"${FUSED_DIR}"

	
    # If remount successful
	if isAlive 'mergerfs' && [[ -f "${FUSED_DIR}/mountcheck" ]]; then
        # Print affirmation and exit script with a code of 0.
		echo -e "\\nSCRIPT: $(S_NAME)\\nTIME: $(TIME)\\nSTATUS: Merging succesful.\\nACTION: Exiting.\\n" >> "${logFile}"
		exit 0
	else
        # If not, print message to log file and exit script with a code of 1.
		echo -e "\\nSCRIPT: $(S_NAME)\\nTIME: $(TIME)\\nSTATUS: Merging failed.\\nACTION: Exiting.\\n" >> "${logFile}"
		exit 1
	fi

}

function unionfsMount () {

    # Location of log file.
    logFile="${UNIONFS_LOGDIR}/$(DAY)-unionfs.log"

    if isAlive 'unionfs-fuse' && [[ -f "${FUSED_DIR}/mountcheck" ]]; then
        # Print affirmation message and exit script with a code of 0.
		echo -e "\\nSCRIPT: $(S_NAME)\\nTIME: $(TIME)\\nSTATUS: Filesystems already merged.\\nACTION: Exiting.\\n" >> "${logFile}"
		exit 0
	else
        # If not, proceed.
		echo -e "\\nSCRIPT: $(S_NAME)\\nTIME: $(TIME)\\nSTATUS: Filesystems not merged.\\nACTION: Merging...\\n" >> "${logFile}"
	fi

    /usr/bin/unionfs-fuse \
        -o allow_other,auto_cache,cow,fsname=UnionFS,hard_remove,noatime,sync_read,umask=0002,use_ino \
        "${LOCAL_DIR}"=RW:"${DRIVE_DIR}"=RO \
        "${FUSED_DIR}"

    # If remount successful
	if isAlive 'unionfs-fuse' && [[ -f "${FUSED_DIR}/mountcheck" ]]; then
        # Print affirmation and exit script with a code of 0.
		echo -e "\\nSCRIPT: $(S_NAME)\\nTIME: $(TIME)\\nSTATUS: Merging succesful.\\nACTION: Exiting.\\n" >> "${logFile}"
		exit 0
	else
        # If not, print message to log file and exit script with a code of 1.
		echo -e "\\nSCRIPT: $(S_NAME)\\nTIME: $(TIME)\\nSTATUS: Merging failed.\\nACTION: Exiting.\\n" >> "${logFile}"
		exit 1
	fi

}
