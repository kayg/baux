# shellcheck source=../def/hash_include.sh
source "${BASH_SOURCE%/*}/../def/hash_include.sh"

function nukeAllChanges () {

    rm -rf "${HOME}/plex"

}

function nukeRclone () {

    rm -rf "${INSTALLDIR}/rclone"
    for dirNum in "${!VAR_RC_MNT[*]}"; do
        rm -rf "${VAR_RC_MNT[${dirNum}]}"
    done

}

function nukePlexdrive () {

    rm -rf "${INSTALLDIR}/plexdrive"
    for dirNum in "${!VAR_PD_MNT[*]}"; do
        rm -rf "${VAR_PD_MNT[${dirNum}]}"
    done

}

function nukeMergerFS () {

    rm -rf "${MERGERFS_LOGDIR}"
    echo -e "Do:\\tsudo apt remove mergerfs\\t to remove the executable from your system."

}

function nukeUnionFS () {

    rm -rf "${UNIONFS_LOGDIR}"
    echo -e "Do:\\tsudo apt remove unionfs-fuse\\t to remove the executable from your system."

}
