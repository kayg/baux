# shellcheck source=../def/hash_include.sh
source "${BASH_SOURCE%/*}/../def/hash_include.sh"

function rcloneUpload () {

    # Set lockFile to prevent additional requests until one is done.
    lockFile="${RCLONE_LOGDIR}/rcloneUpload.lock"

    (

    # Wait for 10 seconds, check if (write) lock is released. If not, exit with a code of 1.
    # -x = write, -w = wait, 10 is the wait interval and 500 is the lock release file descriptor.
    flock -xw 10 768 || (echo -e "\\nSCRIPT: $(S_NAME)\\nTIME: $(TIME)\\nSTATUS: Another move is already in progress.\\nACTION: Exiting." && exit 1)

    # Move locally downloaded files to cloud.
    /usr/bin/rclone move \
        --checkers 4 `# Number of checkers to run in parallel.` \
        --delete-empty-src-dirs \
        --exclude "*partial~" `# Exclude partial files.` \
        --fast-list `# Lesser HTTP transactions at the cost of more memory.` \
        --log-level INFO `# Print necessary messages.` \
        --log-file "${RCLONE_LOGDIR}/$(DAY)-upload.log" \
        --tpslimit 4 `# Limit per minute HTTP transactions.` \
        --transfers 4 `# Limit number of transfers to 4.` \
        "${LOCAL_DIR}" gd: 

    ) 768> "${lockFile}" # Release lock once rclone is done moving.

    return 0
}
