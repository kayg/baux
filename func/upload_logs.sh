# shellcheck source=../def/hash_include.sh
source "${BASH_SOURCE%/*}/../def/hash_include.sh"

function moveRcloneLogs () {

    for logFile in "${RCLONE_LOGDIR}"/*; do
            /usr/bin/rclone copyto \
                --ignore-checksum `# Ignore hash check. Without this flag, the move fails.` \
                --local-no-check-updated `# Don\'t check to see if files are being updated.` \
                --log-level "INFO" \
                --log-file "${RCLONE_LOGDIR}/$(DAY)-upload.log" \
                "${logFile}" "gd:Backup/Logs/Rclone/$(TIME)-$(basename "${logFile}")"
            echo '' > "${logFile}"
    done

}

function movePlexdriveLogs () {

    for logFile in "${PLEXDRIVE_LOGDIR}"/*; do

        /usr/bin/rclone copyto \
            --ignore-checksum `# Ignore hash check. Without this flag, the move fails.` \
            --local-no-check-updated `# Don\'t check to see if files are being updated.` \
            --log-level "INFO" \
            --log-file "${PLEXDRIVE_LOGDIR}/$(DAY)-upload.log" \
            "${logFile}" "gd:Backup/Logs/Plexdrive/$(TIME)-$(basename "${logFile}")"
        echo '' > "${logFile}"
    done

}

function moveMergerFSLogs () {

    for logFile in "${MERGERFS_LOGDIR}"/*; do
        /usr/bin/rclone move \
                --log-level "INFO" \
                --log-file "${RCLONE_LOGDIR}/$(DAY)-upload.log" \
                "${logFile}" "gd:Backup/Logs/MergerFS/"
    done

}

function moveUnionFSLogs () {

    for logFile in "${UNIONFS_LOGDIR}"/*; do
        /usr/bin/rclone move \
                --log-level "INFO" \
                --log-file "${RCLONE_LOGDIR}/$(DAY)-upload.log" \
                "${logFile}" "gd:Backup/Logs/UnionFS/"
    done

}

